# sample-project3

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview in Front-end Folder to check grunt services and error-logs in starting the web-application


1 time for checking system comapatiability
Commands : 
"grunt serve" in front-end

If the server fails to start or you receive any error please check your error log and try to fix using the below web-page. It has many error-reports and a fix for them. 
https://docs.google.com/document/d/1FCf9UQuTe1t_Un_j6o15meV23o9LD6R9uhSKi9-aQLI/edit


Later you can simply run by using ./runfile.sh in annotations folder