'use strict';

/**
 * @ngdoc function
 * @name sampleProject3App.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sampleProject3App
 */
angular.module('sampleProject3App')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
