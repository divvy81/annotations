'use strict';

/**
 * @ngdoc function
 * @name sampleProject3App.controller:ErrorpageCtrl
 * @description
 * # ErrorpageCtrl
 * Controller of the sampleProject3App
 */
angular.module('sampleProject3App')
  .controller('ErrorpageCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
